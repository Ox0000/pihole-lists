# pihole-lists

This project contains some domain lists that can be used with pi-hole. Each list contains comments to describe what they are.

This is not necessarily to block the lot of them, just so that you can add them to the right groups. And then it's up to you to decide what you do with them.

These lists are not intended to be replacing any other lists, only _augment_ them.