#!/usr/bin/env bash

HERE=$(realpath "$(dirname "${0}")")
pushd "${HERE?}" >/dev/null

for f in *.lst; do
    echo -n "Uniqifying \"${f?}\"..."

    _STAGING=$(mktemp)

    grep "^#" "${f}" >"${_STAGING?}"

    grep -v "^#" "${f}" |
        sort \
            --ignore-case \
            --unique \
            >>"${_STAGING?}"

    mv "${_STAGING?}" "${f}"

    echo "DONE"
done

popd >/dev/null
